// netid: amceache
// Alanna McEachen
// mandelmovie.cpp
// Project 03

#include <iostream>
#include "bitmap.h"
#include "mandel.h"
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <vector>
#include <stdio.h>
#include <stdlib.h>

void usage() {
    std::cout << "./mandelmovie MAXC\n";
    std::cout << "\tMAXC\tMaxiumum number of child processes running at one time\n";
    exit(0);
}

bool verifyArgs(char *argv[]) {
    std::cout << strlen(argv[1]) << std::cout;
    for (unsigned int i=0; i < strlen(argv[1]); i++) {
	if ((int)argv[1][i] < 48 || (int)argv[1][i] > 57) {
	    printf("false\n");
	    return false;
	}
    }
    return true;
}

std::string makeMyCommand(double sVal, int picCountInt) {
    std::string picCount = std::to_string(picCountInt);
    std::string mycommand = "./Source/mandel -x 0.287232 -y 0.014287 -s " + std::to_string(sVal) + " -m 1000 -W 3000 -H 3000 -o ./mandeldump/mandel" + picCount + ".bmp";
    //std::string mycommand = "ls -a";
    return mycommand;
}

int main(int argc, char *argv[]) {

    int maxChildren = 2;

    // command line args
    if (argc > 2) {
	usage();
    } else if (argc == 2) {
	if (strcmp(argv[1],"-h") == 0) {
	    usage();
	} else {
	    // make sure we got a number
	    if (verifyArgs(argv)) {
		maxChildren = atoi(argv[1]);
	    } else {
		usage();
	    }
	}
    }

    int totalImages = 50;

    // set up command to be executed
    double sMin = 0.0001;
    double sInc = (2 - sMin)/totalImages;
    std::cout << sInc << std::endl;
    double sVal = 2;
    int picCount = 0;
    std::string mycommand = makeMyCommand(sVal, picCount);

    // main loop setup
    int status;
    std::vector<int> PIDvector;
    bool finished=false;
    while (!finished) {

	if (PIDvector.size() < (unsigned int) maxChildren) {

	    // fork
	    int rc = fork();
	    if (rc < 0) {
    		fprintf(stderr, "fork failed\n");
	        exit(1);
	    } else if (rc == 0) {
	        // child, new process
	        char *cmdargv[100];
	        int cmdLen = mycommand.length();
	        char buffer[cmdLen+1];
	        strcpy(buffer, mycommand.c_str());
	        int cmdargc = 0;
	        char *cmdstr = strtok(buffer, " ");
	        while (cmdstr != NULL) {
	      	    cmdargv[cmdargc++] = strdup(cmdstr);
		    cmdstr = strtok(NULL, " ");
	        }
	        cmdargv[cmdargc] = NULL;

	        // exec	
	        execvp(cmdargv[0], cmdargv);
	    } else {
	        // parent
	        sVal = sVal - sInc;
	        picCount++;
	        mycommand = makeMyCommand(sVal, picCount);
		PIDvector.push_back(rc);

		// if we're just waiting for all children to finish
		if (picCount == totalImages) {
    		    wait(NULL);
		    sleep(1);
		    // since we don't have any more images to make, exit main
		    // loop
		    finished=true;
		}
	    }
	} else if (PIDvector.size() != 0) {
	    // check if child processes are still running
	    std::vector<int>::iterator it = PIDvector.begin();
	    while (it != PIDvector.end()) {
		pid_t endID = waitpid(*it, &status, WNOHANG|WUNTRACED);
		if (endID == -1) {
		    // error calling waitpid
		    printf("waitpid error\n");
		    exit(1);
		} else if (endID == 0) {
		    // still running
		    it++;
		} else if (endID == *it) {
		    // child ended normally
		    it = PIDvector.erase(it);
		}
	    } // end PIDvector check
	} // end if PIDvector size comparison
    } // end main while loop (finished=true)

/*

    // don't actually exit until everything's done running
    while (PIDvector.size() > 0) {
	std::vector<int>::iterator it = PIDvector.begin();
	while (it != PIDvector.end()) {
	    pid_t endID = waitpid(*it, &status, WNOHANG|WUNTRACED);
	    if (endID == -1) {
		printf("waitpid error?\n");
		exit(1);
	    } else if (endID == 0) {
		it++;
	    } else if (endID == *it) {
		it = PIDvector.erase(it);
	    }
	}
    } // end while

*/

    printf("\nGoodbye!\n");

    return 0;
}

