
#include "bitmap.h"
#include <assert.h>
#include <getopt.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

typedef struct __arguments_t {
	struct bitmap *bm;
	double xmin;
	double xmax;
	double ymin;
	double ymax;
	int max;
	int hmin;
	int hmax;
} args_t;

int iteration_to_color( int i, int max );
int iterations_at_point( double x, double y, int max );
void * trash(void * x);
void * thread_compute_image(void * pArgs);
args_t args_create(struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max, int hmin, int hmax);
//void args_delete(struct arguments *a);
void compute_image( struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max, int hmin, int hmax );

void show_help()
{
	printf("Use: mandel [options]\n");
	printf("Where options are:\n");
	printf("-m <max>    The maximum number of iterations per point. (default=1000)\n");
	printf("-n <thrd>   The number of threads to compute the image. (default=1)\n");
	printf("-x <coord>  X coordinate of image center point. (default=0)\n");
	printf("-y <coord>  Y coordinate of image center point. (default=0)\n");
	printf("-s <scale>  Scale of the image in Mandlebrot coordinates. (default=4)\n");
	printf("-W <pixels> Width of the image in pixels. (default=500)\n");
	printf("-H <pixels> Height of the image in pixels. (default=500)\n");
	printf("-o <file>   Set output file. (default=mandel.bmp)\n");
	printf("-h          Show this help text.\n");
	printf("\nSome examples are:\n");
	printf("mandel -x -0.5 -y -0.5 -s 0.2\n");
	printf("mandel -x -.38 -y -.665 -s .05 -m 100\n");
	printf("mandel -x 0.286932 -y 0.014287 -s .0005 -m 1000\n\n");
}

int main( int argc, char *argv[] )
{
	char c;

	// These are the default configuration values used
	// if no command line arguments are given.

	const char *outfile = "mandel.bmp";
	int    threadcount = 1;
	double xcenter = 0;
	double ycenter = 0;
	double scale = 4;
	int    image_width = 500;
	int    image_height = 500;
	int    max = 1000;

	// For each command line argument given,
	// override the appropriate configuration value.

	while((c = getopt(argc,argv,"n:x:y:s:W:H:m:o:h"))!=-1) {
		switch(c) {
			case 'n':
				threadcount = atoi(optarg);
				break;
			case 'x':
				xcenter = atof(optarg);
				break;
			case 'y':
				ycenter = atof(optarg);
				break;
			case 's':
				scale = atof(optarg);
				break;
			case 'W':
				image_width = atoi(optarg);
				break;
			case 'H':
				image_height = atoi(optarg);
				break;
			case 'm':
				max = atoi(optarg);
				break;
			case 'o':
				outfile = optarg;
				break;
			case 'h':
				show_help();
				exit(1);
				break;
		}
	}

	// Display the configuration of the image.
	printf("mandel: x=%lf y=%lf scale=%lf max=%d outfile=%s\n",xcenter,ycenter,scale,max,outfile);

	// Create a bitmap of the appropriate size.
	struct bitmap *bm = bitmap_create(image_width,image_height);

	// Fill it with a dark blue, for debugging
	bitmap_reset(bm,MAKE_RGBA(0,0,255,0));

	// build arguments struct to compute image
	args_t args = args_create(bm, xcenter-scale, xcenter+scale,ycenter-scale,ycenter+scale,max, 0, 249);
	//args_t args2 = args_create(bm, xcenter+1, xcenter+scale,ycenter-scale,ycenter+scale,max);

	// Threading
	pthread_t thread0, thread1;
	int rc;
	rc = pthread_create(&thread0, NULL, thread_compute_image, &args);
	assert(rc == 0);
//	rc = pthread_create(&thread0, NULL, thread_compute_image, &args1);
//	assert(rc == 0);

	printf("doin stuff in my og thread\n");
	// send to compute args in pthread_compute_image
	//thread_compute_image(&args1);
	sleep(5);

	// wait for second thread to finish

	rc = pthread_join(thread0, NULL);
	assert(rc == 0);
//	rc = pthread_join(thread1, NULL);
//	assert(rc == 0);


	// Compute the Mandelbrot image
	//compute_image(bm,xcenter-scale,xcenter+scale,ycenter-scale,ycenter+scale,max);

	// Save the image in the stated file.
	if(!bitmap_save(bm,outfile)) {
		fprintf(stderr,"mandel: couldn't write to %s: %s\n",outfile,strerror(errno));
		return 1;
	}

	return 0;
}

/*
Compute an entire Mandelbrot image, writing each point to the given bitmap.
Scale the image to the range (xmin-xmax,ymin-ymax), limiting iterations to "max"
*/

void *trash(void *x) {
	printf("start\n");
	int *x_ptr = (int *)x;
	++(*x_ptr);
	printf("x in trash %d\n",*x_ptr);
	return NULL;
}

void *thread_compute_image(void *pArgs) {
	// takes in args and throws them into the other function we had set up
	printf("thread compute image\n");
	args_t *args = (args_t *) pArgs;
	
	compute_image(args->bm, args->xmin, args->xmax, args->ymin, args->ymax, args->max, args->hmin, args->hmax);

	return NULL;
}

args_t args_create(struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max, int hmin, int hmax) {
	args_t a;

	a.bm   = bm;
	a.xmin = xmin;
	a.xmax = xmax;
	a.ymin = ymin;
	a.ymax = ymax;
	a.max  = max;
	a.hmin = hmin;
	a.hmax = hmax;

	return a;
}

/*
void args_delete(struct arguments *a) {
	free(a);
}*/

void compute_image( struct bitmap *bm, double xmin, double xmax, double ymin, double ymax, int max, int hmin, int hmax)
{
	int i,j;

	int width = bitmap_width(bm);
	int height = bitmap_height(bm);
	int heightmin = hmin;
	int heightmax = hmax;

	// For every pixel in the image...

	for(j=heightmin;j<heightmax;j++) {

		for(i=0;i<width;i++) {

			// Determine the point in x,y space for that pixel.
			double x = xmin + i*(xmax-xmin)/width;
			double y = ymin + j*(ymax-ymin)/height;

			// Compute the iterations at that point.
			int iters = iterations_at_point(x,y,max);

			// Set the pixel in the bitmap.
			bitmap_set(bm,i,j,iters);
		}
	}
	printf("done?\n");
}

/*
Return the number of iterations at point x, y
in the Mandelbrot space, up to a maximum of max.
*/

int iterations_at_point( double x, double y, int max )
{
	double x0 = x;
	double y0 = y;

	int iter = 0;

	while( (x*x + y*y <= 4) && iter < max ) {

		double xt = x*x - y*y + x0;
		double yt = 2*x*y + y0;

		x = xt;
		y = yt;

		iter++;
	}

	return iteration_to_color(iter,max);
}

/*
Convert a iteration number to an RGBA color.
Here, we just scale to gray with a maximum of imax.
Modify this function to make more interesting colors.
*/

int iteration_to_color( int i, int max )
{
	int gray = 255*i/max;
	return MAKE_RGBA(gray,gray,gray,0);
}




